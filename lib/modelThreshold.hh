#ifndef MODELTHRESHOLD_HH
#define MODELTHRESHOLD_HH

#include <memory>
#include <iostream>      
#include <functional>
#include "vector.hh"
#include "timeprovider.hh"
#include "parameter.hh"
#include "matrix.hh"
#include "discretefunction.hh"
#include<vector>
#include<math.h>
#ifdef OpenCV_FOUND
#include "distancefunction.hh"
#endif

template< const unsigned int mydim >
struct ThresholdModel
  : public ModelDefault< mydim >
{
private:
  using BaseType = ModelDefault< mydim >;

public:
  static const unsigned int dim = BaseType :: dim;
  using RangeVectorType = typename BaseType :: RangeVectorType;

  ThresholdModel( const Mesh& mesh, ConfigParameters& parameters, const TimeProvider& timeProvider )
    : BaseType( parameters, timeProvider ),
      curvatureData_( mesh.N()*dim ),
      curvature_( mesh, SubArray< Vector >( curvatureData_.begin(), curvatureData_.end() ) ),
      positionData_( mesh.N()*dim ),
      position_( mesh, SubArray< Vector >(positionData_.begin(), positionData_.end() ) ),
      N(parameters.get<double>("mesh.N")),
      M(parameters.get<double>("model.beta.M")),
      beta0_( parameters.get<double>( "model.muscle.beta0", M_PI ) ),
      lambda_( parameters.get<double>( "model.muscle.lambda", 2.0 /1.5 ) ),
      omega_( parameters.get<double>( "model.muscle.omega", 2.0 * M_PI ) ),
      eps_( parameters.get<double>( "model.I2.eps" ) ), 
      timeToUpdate(0.0),
      freq(parameters.get<double>("model.beta.freq")*3.3),
      amp(parameters.get<double>("model.beta.amp")),
      wave(parameters.get<double>("model.beta.wave")),
      propFrom(parameters.get<double>("model.read.propFrom")),
      propTo(parameters.get<double>("model.read.propTo")),
      a_(parameters.get<double>("model.torque.a")*3.3),
      b_(parameters.get<double>("model.torque.b")*3.3),
      tau(N),
      dtau(N),
      Switch(2, std::vector<double>(N)), // [0] Dorsal  [1] Ventral
      thd_0(parameters.get<double>("model.threshold.thd_0")),
      thd_1(parameters.get<double>("model.threshold.thd_1")),
      thv_00(parameters.get<double>("model.threshold.thv_00")),
      thv_01(parameters.get<double>("model.threshold.thv_01")),
      thv_10(parameters.get<double>("model.threshold.thv_10")),
      thv_11(parameters.get<double>("model.threshold.thv_11")),
      scalarCurvature(N),
      propFeedback(N),
      propFeedbackHeadDorsal(N),
      propFeedbackHeadVentral(N),
      ginput(4),
      cinput(4),
      lastFB(N),
      //nu_out(NULL),
      reset(parameters.get<bool>("model.threshold.reset")),
      Activation(NULL),
      Beta(NULL),
      Kappa(NULL),
      feedback(NULL),
      //feedbackHead(NULL),
      ginputFile(NULL),
      cinputFile(NULL),
      ystasmddFile(NULL),
      ystarmddFile(NULL),
      ystasmdvFile(NULL),
      ystarmdvFile(NULL),
      sigystasmddFile(NULL),
      sigystarmddFile(NULL),
      sigystasmdvFile(NULL),
      sigystarmdvFile(NULL),
      dtauHeadFile(NULL),
      dtauBodyFile(NULL),
      pointx(NULL),
      pointy(NULL),
      outputs(4)
 {
	for(int j=0;j<N;j++)
	{
		Switch[0][j] = 0;
		Switch[1][j] = 0; //0
	}
        Activation = new std::ofstream("Activation.txt");
	if( !Activation->is_open() )
        {
          std::cerr << "Error: Activation.txt didn't open" << std::endl;
        }
        Beta = new std::ofstream("Beta.txt");
	if( !Beta->is_open() )
        {
          std::cerr << "Error: Beta didn't open" << std::endl;
        }
	Kappa = new std::ofstream("Kappa.txt");
        if( !Kappa->is_open() )
        {
          std::cerr << "Error: Kappa didn't open" << std::endl;
        }
	feedback = new std::ofstream("feedback.txt");
	if( !feedback->is_open() )
	{
  	  std::cerr << "Error: feedback file didn't open" << std::endl;
	}
  /*
  feedbackHead = new std::ofstream("feedbackHead.txt");
	if( !feedbackHead->is_open() )
	{
  	  std::cerr << "Error: feedbackHead file didn't open" << std::endl;
	}
  */

  cinputFile = new std::ofstream("cinput.txt");
	if( !cinputFile->is_open() )
	{
  	  std::cerr << "Error: cinput file didn't open" << std::endl;
	}

  ginputFile = new std::ofstream("ginput.txt");
	if( !ginputFile->is_open() )
	{
  	  std::cerr << "Error: ginput file didn't open" << std::endl;
	}

  ystasmddFile = new std::ofstream("ystaSMDD.txt");
	if( !ystasmddFile->is_open() )
	{
  	  std::cerr << "Error: ystaSMDD file didn't open" << std::endl;
	}

  ystarmddFile = new std::ofstream("ystaRMDD.txt");
	if( !ystarmddFile->is_open() )
	{
  	  std::cerr << "Error: ystaRMDD file didn't open" << std::endl;
	}

  ystasmdvFile = new std::ofstream("ystaSMDV.txt");
	if( !ystasmdvFile->is_open() )
	{
  	  std::cerr << "Error: ystaSMDV file didn't open" << std::endl;
	}

  ystarmdvFile = new std::ofstream("ystaRMDV.txt");
	if( !ystarmdvFile->is_open() )
	{
  	  std::cerr << "Error: ystaRMDV file didn't open" << std::endl;
	}

  sigystasmddFile = new std::ofstream("sigystaSMDD.txt");
	if( !sigystasmddFile->is_open() )
	{
  	  std::cerr << "Error: sigystaSMDD file didn't open" << std::endl;
	}

  sigystarmddFile = new std::ofstream("sigystaRMDD.txt");
	if( !sigystarmddFile->is_open() )
	{
  	  std::cerr << "Error: sigystaRMDD file didn't open" << std::endl;
	}

  sigystasmdvFile = new std::ofstream("sigystaSMDV.txt");
	if( !sigystasmdvFile->is_open() )
	{
  	  std::cerr << "Error: sigystaSMDV file didn't open" << std::endl;
	}

  sigystarmdvFile = new std::ofstream("sigystaRMDV.txt");
	if( !sigystarmdvFile->is_open() )
	{
  	  std::cerr << "Error: sigystaRMDV file didn't open" << std::endl;
	}

  dtauHeadFile = new std::ofstream("dtauHead.txt");
	if( !dtauHeadFile->is_open() )
	{
  	  std::cerr << "Error: dtauHead file didn't open" << std::endl;
	}

  dtauBodyFile = new std::ofstream("dtauBody.txt");
	if( !dtauBodyFile->is_open() )
	{
  	  std::cerr << "Error: dtauBody file didn't open" << std::endl;
	}

	pointx = new std::ofstream("pointx");
	if( !pointx->is_open() )
	{
  	  std::cerr << "Error: pointx file didn't open" << std::endl;
	}
	pointy = new std::ofstream("pointy");
	if( !pointy->is_open() )
	{
  	  std::cerr << "Error: pointy file didn't open" << std::endl;
	}

  for(int i = 0; i <= 3; i++) {
    outputs[i] = sigmoid(yStates[i] + biases[i]);
  }
 }


~ThresholdModel() // destructor causes core dump for some reason :s 
{
  Activation->close();
  //if( Activation )
  //  delete Activation;
  Beta->close();
  //if( Beta )
  //  delete Beta;
  Kappa->close();
  //if( Kappa )
  //  delete Kappa;
  feedback->close();
  //feedbackHead->close();
  cinputFile->close();
  ginputFile->close();
  ystasmddFile->close();
  ystarmddFile->close();
  ystasmdvFile->close();
  ystarmdvFile->close();
  sigystasmddFile->close();
  sigystarmddFile->close();
  sigystasmdvFile->close();
  sigystarmdvFile->close();
  dtauHeadFile->close();
  dtauBodyFile->close();
  pointx->close();
  pointy->close();
}

  virtual double e( const double s ) const
  {
    const double eps = eps_;
    const double num = 2.0 * std::sqrt( (eps + s)*(eps + 1.0 - s) );
    const double den = 1.0 + 2.0 * eps;

    return BaseType::e(s) * ( num*num*num ) / ( den*den*den ); // keep in mind Ma = -E*I2*beta still <<<<<<<<<<<<<<<<

  }

  virtual double rad( const double s ) const
  {
    const double eps = eps_;
    const double num = 2.0 * std::sqrt( (eps + s)*(eps + 1.0 - s) );
    const double den = 1.0 + 2.0 * eps;

    return ( num*num*num ) / ( den*den*den ); // keep in mind Ma = -E*I2*beta still <<<<<<<<<<<<<<<<

  }

  virtual RangeVectorType X0( const double s ) const
  {
    RangeVectorType ret;
	ret[0] = s; 		  //cos(-M_PI*s)/M_PI;
	ret[1] = 0.0; //0.0; //sin(M_PI*s)/M_PI; //s*(1.-s)

    for( unsigned int d = 2; d < dim; ++d )
      {
	ret[ d ] = 0.0;
      }

    return ret;
  }

  using BaseType::f;
  using BaseType::gamma;
  using BaseType::K;
  using BaseType::e;

  using CurvatureType = PiecewiseLinearFunction<dim>;
  Vector curvatureData_;
  CurvatureType curvature_;

  void storeCurvature( const CurvatureType& curvature )
  {
    curvature_.assign( curvature );
  }

  using PositionType = PiecewiseLinearFunction<dim>;
  Vector positionData_;
  PositionType position_;

  void storePosition( const PositionType& position )
  {
    position_.assign( position );
  }  

mutable double N;
mutable double M;

  void write_Activation()
  {

	*Activation << " " <<  timeProvider().time();

	for(int j=0; j < N; j++) // technically j < M, but for now (continuous torque), M=N effectively
	{
		double pos = double(j)/double(N);
		*Activation << " " << Switch[0][j] - Switch[1][j]; //
	}
	
	*Activation << "\n";

  }

  void write_beta()
  {

	*Beta << " " <<  timeProvider().time();

	for(int j=0; j < N; j++)
	{
		*Beta << " " << tau[j];
	}
	
	*Beta << "\n";

  }


  void write_kappa()
  {

        *Kappa << " " <<  timeProvider().time();

        for(int j=0; j < N; j++)
        {
        	*Kappa << " " << scalarCurvature[j];
	}

        *Kappa << "\n"; 
  }

  void write_feedback()
  {

        *feedback << " " <<  timeProvider().time();

        for(int j=0; j < N; j++)
        {
		double pos = double(j)/double(N);
		*feedback << " " << propFeedback[j] ; 
	}

        *feedback << "\n"; 
  }
  /*
  void write_feedbackHead()
  {

        *feedbackHead << " " <<  timeProvider().time();

        for(int j=0; j < N; j++)
        {
		//double pos = double(j)/double(N);
		*feedbackHead << " " << propFeedbackHead[j] ; 
	}

        *feedbackHead << "\n"; 
  }
  */

  void write_ginput()
  {

        *ginputFile << " " <<  timeProvider().time();

        for(int i = 0; i < 4; i++) {
          *ginputFile << " " << ginput[i];
        }

		//double pos = double(j)/double(N);
		 

        *ginputFile << "\n"; 
  }

  void write_cinput()
  {

        *cinputFile << " " <<  timeProvider().time();


		    for(int i = 0; i < 4; i++) {
          *cinputFile << " " << cinput[i];
        }


        *cinputFile << "\n"; 
  }

  void write_ystasmdd()
  {

        *ystasmddFile << " " <<  timeProvider().time();

		//double pos = double(j)/double(N);
		*ystasmddFile <<" " <<ystasmdd ; 

        *ystasmddFile << "\n"; 
  }

  void write_ystarmdd()
  {

        *ystarmddFile << " " <<  timeProvider().time();


		//double pos = double(j)/double(N);
		*ystarmddFile << " "<<ystarmdd ; 


        *ystarmddFile << "\n"; 
  }

  void write_ystasmdv()
  {

        *ystasmdvFile << " " <<  timeProvider().time();


		//double pos = double(j)/double(N);
		*ystasmdvFile << " "<< ystasmdv ; 


        *ystasmdvFile << "\n"; 
  }

  void write_ystarmdv()
  {

        *ystarmdvFile << " " <<  timeProvider().time();


		//double pos = double(j)/double(N);
		*ystarmdvFile << " "<< ystarmdv; 


        *ystarmdvFile << "\n"; 
  }

  void write_sigystasmdd()
  {

        *sigystasmddFile << " " <<  timeProvider().time();


		//double pos = double(j)/double(N);
		*sigystasmddFile << " "<<sigystasmdd ; 


        *sigystasmddFile << "\n"; 
  }

  void write_sigystarmdd()
  {

        *sigystarmddFile << " " <<  timeProvider().time();


		//double pos = double(j)/double(N);
		*sigystarmddFile << " "<<sigystarmdd ; 


        *sigystarmddFile << "\n"; 
  }

  void write_sigystasmdv()
  {

        *sigystasmdvFile << " " <<  timeProvider().time();


		//double pos = double(j)/double(N);
		*sigystasmdvFile << " "<< sigystasmdv; 


        *sigystasmdvFile << "\n"; 
  }

  void write_sigystarmdv()
  {

        *sigystarmdvFile << " " <<  timeProvider().time();


		//double pos = double(j)/double(N);
		*sigystarmdvFile << " "<<sigystarmdv; 


        *sigystarmdvFile << "\n"; 
  }



  void write_pointx()
  {

        *pointx << " " <<  timeProvider().time();

        for(int j=0; j < N; j++)
        {
		*pointx << " " << position_.evaluate(j)[0] ; 
	}

        *pointx << "\n"; 
  }

  void write_pointy()
  {

        *pointy << " " <<  timeProvider().time();

        for(int j=0; j < N; j++)
        {
		*pointy << " " << position_.evaluate(j)[1] ; 
	}

        *pointy << "\n"; 
  }



 void updateScalarCurvature() const // BUG: acts on internal meshpoints only!! (j != 0,N-1)
  {
	assert(scalarCurvature.size() == N);

	for(unsigned int j=1; j < N-1; j++ )
	{

		const auto xem = position_.evaluate( j-1 );
		const auto xe  = position_.evaluate( j  );
		const auto xep = position_.evaluate( j+1 );
	
		RangeVectorType num = (xem - xe).perp();
		RangeVectorType nup = (xe - xep).perp();
		const auto a = num.norm();
		const auto b = nup.norm();
		num[0] = num[0]/a;
		num[1] = num[1]/a;
		nup[0] = nup[0]/b;
		nup[1] = nup[1]/b;

		RangeVectorType nubar = num + nup;
		const auto c = nubar.norm();
		nubar[0] = nubar[0]/c;
		nubar[1] = nubar[1]/c;
		scalarCurvature[j] = curvature_.evaluate(j)[0]*nubar[0] + curvature_.evaluate(j)[1]*nubar[1];
	}
	
	scalarCurvature[0]   = scalarCurvature[1];    // -beta(0)
	scalarCurvature[N-1] = scalarCurvature[N-2];  // -beta(1)
  }

  
  void updatePropFeedback() const
   {
	double sum;
	int S;
	double a;
	double b;
	int ia;
	int ib;
	for(double s = 0.0 ; s <= 1.0 ; s = s + 1/N)
	{	
		S = round(s*(N-1));
		a = std::max(s + propFrom, 0.0);
		a = std::min(a, 1.0);
		b = std::min(s + propTo,   1.0 + 1.0/N); // so ia-ib != 0
		ia = a*N;
		ib = b*N;
		sum = 0.0;

		for(int i = ia ; i < ib ; ++i)
		{
			if( i < 1 || i > N-1 )
			{
				sum = sum + 0;
			}
			else
			{
				sum = sum + scalarCurvature[i];
			} 
		}
		propFeedback[S] = sum/std::abs(double(ib - ia)); // *5/N
	}
   }
  
   void updatePropFeedbackHead() const
   {
	double sum;
	int S;
	double a;
	double b;
	int ia;
	int ib;
  double theta;
  double radius;
  double width = 0.04;
  double d;
  double v;
	for(double s = 0.0 ; s <= 1.0 ; s = s + 1/N)
	{
		
		S = round(s*(N-1));
		a = std::max(s + propFrom, 0.0);
		a = std::min(a, 1.0);
		b = std::min(s + propTo,   1.0 + 1.0/N); // so ia-ib != 0
		ia = a*N;
		ib = b*N;
		sum = 0.0;

		for(int i = ia ; i < ib ; ++i)
		{
			if( i < 1 || i > N-1 )
			{
				sum = sum + 0;
			}
			else
			{
				sum = sum + scalarCurvature[i];
			} 
		}
    //calculate theta
    theta = 0.00781*sum;
    radius = 1/sum;

    d = theta * (radius + (width/2));
    v = theta * (radius - (width/2));
    //Dorsal
    if(d != d || v != v) {
      propFeedbackHeadDorsal[S] = 0.0;
      propFeedbackHeadVentral[S] = 0.0;
    }else{
      //Dorsal
      propFeedbackHeadDorsal[S] = theta * (radius + (width/2)); // *5/N   std::abs(double(ib - ia))
      
      //Ventral
      propFeedbackHeadVentral[S] = theta * (radius - (width/2));
    }
  
    //std::cout<<propFeedbackHeadDorsal[S]<<" | "<<propFeedbackHeadVentral[S]<<std::endl;
	}
   }
   

  /*
  void intializeBodyConstants() const {
    double NS2 = N_segments/2.0;
    double LS2 = L_seg*L_seg;
    double r;
    for(int i = 0; i <= N_rods + 1; i++) {
      R[i] = R_min * fabs(sin(acos((i - NS2)/(NS2 + 0.2))));
    }

    for(int i = 0; i <= N_segments; i++) {
      r = R[i] - R[i+1];
      L_L0[i] = sqrt(LS2+r*r);
    }
  }
  
  void UpdateKinematics() const{
    for(int i = 0; i <= N_rods; i++) {
      int i3 = 3*i;

      double x = Z[i3];
      double y = Z[i3+1];
      double phi = Z[i3+2];
      
      sinPhi[i] = sin(phi);
      cosPhi[i] = cos(phi);

      p_D_x[i] = x + R[i]*cosPhi[i];
      p_D_y[i] = y + R[i]*sinPhi[i];
      p_V_x[i] = x - R[i]*cosPhi[i];
      p_V_y[i] = y - R[i]*sinPhi[i];

    }
  }

  void updatePropFeedbackHead() const
   {
	double sum;
	int S;
	double a;
	double b;
	int ia;
	int ib;
  
  double ds;
  double vs;

  for(int i = 0; i <= N_rods; i++) {
    int i3 = 3*i;

    Z[i3] = i*L_seg;
    Z[i3+1] = 0.0;
    Z[i3+2] = M_PI/2;
  }

  UpdateKinematics();

  for(int i = 0; i < 49; i++) {
    int i1 = i+1;
    D_x = p_D_x[i1] - p_D_x[i];
    D_y = p_D_y[i1] - p_D_y[i];
    V_x = p_V_x[i1] - p_V_x[i];
    V_y = p_V_x[i1] - p_V_x[i];

    L_D_L[i] = sqrt(D_x*D_x + D_y*D_y);

    L_V_L[i] = sqrt(V_x*V_x + V_y*V_y);
  }
  
  for(int i = 0; i < N_segments; i++) {
    ds = (L_D_L[i] - L_L0[i])/L_L0[i];
    vs = (L_V_L[i] - L_L0[i])/L_L0[i];
    normSegLenD[i] = ds;
    normSegLenV[i] = vs;
  }

  double d = 0.0;
  double v = 0.0;
  for(int i = 7; i < 21; i++) {
    d += normSegLenD[i];
    v += normSegLenV[i];
    
  }

  //Dont know what SRheadgain is, being set in worm.cpp but can't find actual number
  externalinput[SMDD] = -120.535*(d/NSEGSHEAD);
  externalinput[SMDV] = -120.535*(v/NSEGSHEAD);
  //std::cout<<externalinput[SMDD]<<std::endl;

  /*
	for(double s = 0.0 ; s <= 1.0 ; s = s + 1/N)
	{
		
		S = round(s*(N-1));
		a = std::max(s + propFrom, 0.0);
		a = std::min(a, 1.0);
		b = std::min(s + propTo,   1.0 + 1.0/N); // so ia-ib != 0
		ia = a*21;
		ib = b*21;
		sum = 0.0;

		for(int i = ia ; i < ib ; ++i)
		{
			if( i < 1 || i > N-1 )
			{
				sum = sum +  0;
			}
			else
			{
				sum = sum + scalarCurvature[i];
			} 
		}
		propFeedbackHead[S] = sum/std::abs(double(ib - ia)); // *5/N
	}
 
   }
  */


/*
// conserve prop range along all body -- no scaling required
  void updatePropFeedback() const
   {
        double sum;
        int S;
        double a;
        double b;
        int ia;
        int ib;
        for(double s = 0.0 ; s <= 1.0 ; s = s + 1/N)
        {
                S = round(s*(N-1));
		double propMin = 0.25 ;// propTo - propFrom; //  >> if propRange > propMin, range shrinks slightly towards tail, but not completely
                a = std::max(s + propFrom, 0.0);
                a = std::min(a, 1.0 - (propMin)); // prop range fixed towards tail
                b = std::min(s + propTo,   1.0 + 1.0/N); // so ia-ib != 0
                ia = a*N;
                ib = b*N;
                sum = 0.0;
                for(int i = ia ; i < ib ; ++i)
                {
                        if( i < 1 || i > N-1 )
                        {
                                sum = sum +  0;
                        }
                        else if(i < S) // range becomes anterior to s
			{
				sum = sum - scalarCurvature[i];
			}
			else
                        {
                                sum = sum + scalarCurvature[i];
                        }
                }
                propFeedback[S] = sum/std::abs(double(N*(b - a))); // *N
        }
   }
*/
void printScalarCurvature() const
  {

	std::cout << timeProvider().time() << " , " <<  scalarCurvature.size()  << " curve: ";
	for(unsigned int k = 0; k < N ; k++  )
	{
	 std::cout << scalarCurvature[ k ] << " ,  " ;	
	}
	std::cout << std::endl;
  }

void printPropFeedback() const
  {

	std::cout << timeProvider().time() << " , (tau[k]) ";
	for(unsigned int k = 0; k < N ; k++  )
	{
	 std::cout << propFeedback[ k ] << " ,  " << tau[ k ];	
	}
	std::cout << std::endl;
  }

void printSwitch() const
  {
	std::cout << "time: " << timeProvider().time() << std::endl;
	std::cout << "Dorsal:   ";	
	for(int j=90; j<N; j++)
	{
		std::cout << Switch[0][j] << " ";
	}

	std::cout << std::endl;
	std::cout << "Ventral:  ";	
	for(int j=90; j<N; j++)
	{
		std::cout << Switch[1][j] << " ";
	}
	std::cout << std::endl;

  }

  double activation( const double s ) const
   {

	// Output travelling sine wave
	const double t = timeProvider().time();
	const double q = 2.0 * M_PI * s/wave - 2.0* M_PI *freq*t;
	double output = amp * sin( q ); // boundary(s)*

	return output;
   }
/*
  void threshold_FFD(const double s) const // When driving by noramlised sinewave
   {
	int S = round(s*(N-1));
	
	int thd_on  = .7 ; // +ve
	int thd_off = .2 ;
	int thv_on  = -.6 ; // -ve
	int thv_off = -.25 ;

	// Dorsal
	if( Switch[0][S] < 1e-10 && activation(s) >= thd_on )
		{
			Switch[0][S] = 1;
		}
	else if ( std::abs(Switch[0][S] - 1 ) < 1e-10 && activation(s) < thd_off )
		{
			Switch[0][S] = 0;
		}

	// Ventral
	if( Switch[1][S] < 1e-10 && activation(s) <= thv_on ) // thresh has opposite signs than dorsal, since curvature has flipped sign
		{
			Switch[1][S] = 1;
		}
	else if ( std::abs(Switch[1][S] - 1 ) < 1e-10 && activation(s) > thv_off )
		{
			Switch[1][S] = 0;
		}
	
   }
*/

  void threshold_prop() const// When proprioception drives locomotion
   {

       for(int j=0; j<N; ++j)
        {
        	if( propFeedback[j] < thd_0 ) /**/ { Switch[0][j] = 1;}
        	else if( propFeedback[j] > thd_1  ) /**/ { Switch[0][j] = 0;}
        	Switch[1][j] = 1-Switch[0][j];
	}

   }

  
  virtual double beta( const double s ) const
  {

	int S = round(s*(N-1));
  double ans1 = 0.0;
  double ans2 = 0.0;
  //std::cout<< S<<" | "<<"States: "<< yStates[0]<<" | " <<yStates[1]<<" | " <<yStates[2]<<" | " <<yStates[3]<<std::endl;
  double dorsalHeadInput = 0.0;
  double ventralHeadInput = 0.0;
  //std::cout<< "DO I GET HERE***"<< std::endl;
//	std::cout  << S << " tau: " << -b_*tau[S] <<  " , curve: " << a_*scalarCurvature[ S ]  << " , prop: " << propStrength*propFeedback[ S ]  << std::endl
  if(S < 42) {

    izquierdoHeadCircuit(s);

    dorsalHeadInput = outputs[SMDD] + outputs[RMDV];
    ventralHeadInput = outputs[SMDV] + outputs[RMDD];
    //dorsalHeadInput = outputs[SMDD] + outputs[RMDD];
    //ventralHeadInput = outputs[SMDV] + outputs[RMDV];
    //double headBeta = dorsalHeadInput - ventralHeadInput;
    //std::cout<<dorsalHeadInput<<std::endl;
    //std::cout<<dorsalHeadInput-ventralHeadInput<<std::endl;
    //dorsalHeadInput = outputs[SMDD];
    //ventralHeadInput = outputs[SMDV];

    //std::cout<<outputs[0]<<" | "<<outputs[1]<<" | "<<outputs[2]<< " | "<<outputs[3]<<std::endl; 
    //std::cout<< "dorsal Head: " << dorsalHeadInput<< "ventral Head: " << ventralHeadInput<< "Gains: " << nmj_gain[S] << std::endl;
    //dtau[S] = 0;
    //std::cout<< S<<" | dorsal Head: " << dorsalHeadInput<< "| ventral Head: " << ventralHeadInput<< "| Gains: " << nmj_gain[S] << std::endl;

    //dtau[S] = result;
    /*
    if(headBeta < 1) {
      //std::cout<<"***ALEX***"<<std::endl;
      dtau[S] = headBeta * -600;
    }else{
      //std::cout<<"Here Now"<<std::endl;
      dtau[S] = headBeta * 300;
    }
    */
    
    
    //dtau[S] = dorsalHeadInput - ventralHeadInput;
    //std::cout<<dtau[S]<<std::endl;
    dtau[S] = -a_*(Switch[0][S] - Switch[1][S]) - b_*tau[S];//dtau[S] = 2.5; Adding tau[S] turns it to 0 every value
    *dtauHeadFile << " " <<  timeProvider().time();
    *dtauHeadFile << " " << dtau[S]; 
    *dtauHeadFile << "\n"; 

    //std::cout<< "Alex HEAD: "<< dtau[S] << " |  S: "<<S<< std::endl;

    
    /*
    if(S == 0) {
      ans1 = dtau[S];
      ans2 = -a_*(Switch[0][S] - Switch[1][S]) - b_*tau[S];
      std::cout<< "**FIRST INSTANCE HEAD Izquierdo: "<< ans1 << " | Jack: "<<ans2<< std::endl;
    }else{
      std::cout<< "HEAD: "<< dtau[S]<< std::endl;
    }
    */
   //std::cout<< "tau: "<< tau[S]<< std::endl;

  }else{
    dtau[S] =  -a_*(Switch[0][S] - Switch[1][S]) - b_*tau[S]; // +ve curvature feedback (due to weird -ve beta to kappa relationship - it's actually inhibitory feedback)
    *dtauBodyFile << " " <<  timeProvider().time();
    *dtauBodyFile << " " << dtau[S]; 
    *dtauBodyFile << "\n"; 

  }
	
	if(timeToUpdate <= timeProvider().time())
	{
		for(int k=0; k<N; k++)
		{
      tau[k] = tau[k] + timeProvider().deltaT()*dtau[k];	 
		}
	timeToUpdate = timeToUpdate + timeProvider().deltaT();
	}

  //propFeedbackWave[waveCount] = tau[S];
  //waveCount += 1;
	return tau[S];   //tau[S]; //activation(s);
  }
  /*
  void waveCalculation() const {
    waveFeedback.open("wave.txt");
    for(int i = 0; i <; i++) {
      waveFeedback << propFeedbackWave[i]<<std::endl;
    }
    waveFeedback.close();
  }
  */

  // The sigmoid function
  inline double sigmoid(double x) const {
    return 1/(1 + exp(-x));
  }

  //ALM: This function does what threshold prop does for beta, but instead this does it for delta. Solve equation and return this value in beta function
  //ALM: S in beta returns the point in the body it is at

  void izquierdoHeadCircuit( const double s ) const {
    int S = round(s*(N-1));
    //std::cout<<propFeedbackHeadDorsal[S]<<" | "<<propFeedbackHeadVentral[S]<<std::endl;
    //std::cout<<propFeedbackHead[S]<<std::endl;
    
    externalinput[0] = -120.535*propFeedbackHeadDorsal[S];
    externalinput[2] = -120.535*propFeedbackHeadVentral[S];
    //std::cout<<"New Iteration"<<std::endl;
    //std::cout<<externalinput[0]<<" | "<<externalinput[2]<<" | "<<yStates[0]<<" | "<<yStates[1]<<" | "<<yStates[2]<<" | "<<yStates[3]<<std::endl;
    // Update past states (used for gap junctions) and set time constants
    for (int i = 0; i <= Neurons; i++){
        yPastStates[i] = yStates[i];
        RtimeConstant[i] = 1/timeConstant[i];
        //std::cout<<yPastStates[i]<<yStates[i]<<std::endl;
    }
    //std::cout<<RtimeConstant[0]<<std::endl;
    
    //std::cout<<" | "<<RtimeConstant[0]<<" | "<<RtimeConstant[1]<<" | "<<RtimeConstant[2]<<" | "<<RtimeConstant[3]<<std::endl;
    // Update the state of all neurons.
    for (int i = 0; i <= Neurons; i++) {
        // External input
        double input = externalinput[i];
        //std::cout<<input<<std::endl; 
        //std::cout<<externalinput[i]<<std::endl;
        double functionCinput = 0.0;
        double functionGinput = 0.0;
        
        // Input from chemical synapses
        for (int j = 0; j <= Neurons; j++) {
          input += w[i][j] * outputs[j];
          functionCinput += w[i][j] * outputs[j];
        }
           
        cinput[i] += functionCinput;
        // Input from electrical synapses
        for (int j = 0; j <= Neurons; j++) {
          input += g[i][j] * (yPastStates[j] - yPastStates[i]);
          functionGinput += g[i][j] * (yPastStates[j] - yPastStates[i]);
        }
        ginput[i] = functionGinput;  
        // Take the step (step size constant at 0.01)
        yStates[i] += 0.01 * RtimeConstant[i]* (input - yStates[i]); //* Found out what Rtaus is, time constant
        //std::cout<<yStates[i]<<std::endl;
        //std::cout<< "S: " << s <<"| STATES: "<<yStates[i] << " | input: "<< input << std::endl;
        //result += input;
    }
    // Update the outputs of all neurons.
    for (int i = 0; i <= Neurons; i++){
      outputs[i] = (sigmoid(1 * (yStates[i] + biases[i]))) - 0.5; //sigmoid(gains[i] * (yStates[i] + biases[i]));
    }

    //outputs[RMDD] = outputs[SMDV];
    //outputs[RMDV] = outputs[SMDD];

    //std::cout<<"Outputs: | "<<outputs[0]<<" | "<<outputs[1]<<" | "<<outputs[2]<<" | "<<outputs[3]<<std::endl;
    ystasmdd = yStates[0];//state of SMDD
    ystarmdd = yStates[1];//state of RMDD
    ystasmdv = yStates[2];//state of SMDV
    ystarmdv = yStates[3];//state of RMDV

    sigystasmdd = outputs[0];//sigmoid for SMDD
    sigystarmdd = outputs[2];//sigmoid for RMDD
    sigystasmdv = outputs[2];//sigmoid for SMDV
    sigystarmdv = outputs[0];//sigmoid for RMDV
    //std::cout<<propFeedbackHead[S]<<std::endl;

    //std::cout<<propFeedback[S]<<std::endl;
    //The states are making this repeat, theyre updating and incrementing slowly but repeating
    //outputs reaches a point where it stagnates and stays the same
    //std::cout<< "S: " << S <<"| STATES: "<<yStates[0]<< " | "<<yStates[1] << " | "<<yStates[2]<< " | "<<yStates[3] << " | output: "<< outputs[0]<< " | "<<outputs[1] << " | "<<outputs[2]<< " | "<<outputs[3] << std::endl;

    //std::cout<<"External inputs: "<<externalinput[0]<<" | "<<externalinput[1]<<" | "<<externalinput[2]<<" | "<<externalinput[3]<<std::endl;
    /* First try
    for(int i = 0; i <= Neurons; i++) {
      input += yVal[i];
      for(int j = 0; j <= Neurons; j++) {
        input += w[i][j] * sigmoid(yVal[j] + biases[j]);
        input += g[i][j] * (yVal[j] - yVal[i]);
      }
    }
    input += rVal*propFeedback[S];
    */
    //This for loop covers the second summation symbol
    /*
    for(int i = 0; i <= Neurons; i++) {
      elecOutput[i] = yVal[i] - yVal[0];
    }
    */
    /*
    for(int i = 0; i <= Neurons; i++) {
      yVal[i] = S * (1/S) * ((chemicalSum+electricalSum) - pastYVal[i]);
    }
    */
    //answer = -yVal[0] + chemicalSum + electricalSum + (rVal*propFeedback[S]);//scalarCurvature: No, Prop Feedback is what's making this repeat 
  }

using BaseType::timeProvider;



private:
  const double beta0_, lambda_, omega_;
  const double eps_;
  mutable double timeToUpdate;
  const double freq, amp, wave;
  const double propFrom, propTo;
  const double a_, b_;
  mutable std::vector<double> tau;
  mutable std::vector<double> dtau;
  mutable std::vector<std::vector<double>> Switch;
  const double thd_0, thd_1, thv_00, thv_01, thv_10, thv_11;
  mutable std::vector<double> scalarCurvature;
  mutable std::vector<double> propFeedback;
  mutable std::vector<double> propFeedbackHeadDorsal;
  mutable std::vector<double> propFeedbackHeadVentral;
  mutable std::vector<double> lastFB;
  mutable bool reset;
	//std::ofstream* nu_out;
	mutable std::ofstream* Activation;
	mutable std::ofstream* Beta;
	mutable std::ofstream* Kappa;
	mutable std::ofstream* feedback;
	mutable std::ofstream* pointx;
	mutable std::ofstream* pointy;
  //mutable std::ofstream* feedbackHead;
  mutable std::ofstream* ginputFile;
  mutable std::ofstream* cinputFile;
  mutable std::ofstream* ystasmddFile;
  mutable std::ofstream* ystarmddFile;
  mutable std::ofstream* ystasmdvFile;
  mutable std::ofstream* ystarmdvFile;
  mutable std::ofstream* sigystasmddFile;
  mutable std::ofstream* sigystarmddFile;
  mutable std::ofstream* sigystasmdvFile;
  mutable std::ofstream* sigystarmdvFile;
  mutable std::ofstream* dtauHeadFile;
  mutable std::ofstream* dtauBodyFile;
  mutable std::vector<double> ginput;
  mutable std::vector<double> cinput;
  mutable double ystasmdd;
  mutable double ystarmdd;
  mutable double ystasmdv;
  mutable double ystarmdv;
  mutable double sigystasmdd;
  mutable double sigystarmdd;
  mutable double sigystasmdv;
  mutable double sigystarmdv;

  //ALM: Neurons
  const int SMDD = 0;
  const int RMDD = 1;
  const int SMDV = 2;
  const int RMDV = 3;

  double w[4][4] = {{-14.9121, 0.0, -11.2755, 14.9933},
                    {0.0, 6.62512, 0.0, -11.6075},
                    {-11.2755, 14.9933, -14.9121, 0.0}, 
                    {0.0, -11.6075, 0.0, 6.62512}};
  
  double g[4][4] = {{0.0, 0.0199558, 0.0, 0.0},
                    {0.0, 0.0, 0.0, 0.0},
                    {0.0, 0.0, 0.0, 0.0199558},
                    {0.0, 1.51095, 0.0, 0.0}};


  mutable double yStates[4] = {0.0, 0.0, 0.0, 0.0};
  mutable double yPastStates[4] = {0.0, 0.0, 0.0, 0.0};

  mutable double timeConstant[4] = {0.5, 0.5, 0.5, 0.5};
  mutable double RtimeConstant[4] = {0.0, 0.0, 0.0, 0.0};
  

  mutable double biases[4] = {6.27, -3.65, 6.27,-3.65};//SMDD, RMDD, SMDV, RMDV?
  double rVal[4] = {-120.535, 0.0, -120.535, 0.0};//-50.5, better but still terrible

  const int Neurons = 3;

  mutable std::vector<double> outputs;

  // NMJ Weights
  const double nmj[4] = {0.000371977, 1.0, 0.000371977, 1.0};

  // NMJ Gain
  const double NMJ_Gain_Map = 0.5;
  mutable double nmj_gain[10] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  mutable double result = 0.0;

  //Stretch Receptor Head variables
  const int N_rods = 50;
  const int N_segments = 50;
  const int R_min = 40.0e-6;
  mutable double R[50];

  mutable double sinPhi[50];
  mutable double cosPhi[50];

  mutable double D_x;
  mutable double D_y;
  mutable double V_x;
  mutable double V_y;

  mutable double p_D_x[50];
  mutable double p_D_y[50];
  mutable double p_V_x[50];
  mutable double p_V_y[50];

  mutable double Z[153];

  const double L_worm = 1.0e-3;
  const double L_seg = L_worm/N_segments;

  mutable double L_D_L[50];
  mutable double L_V_L[50];

  mutable double L_L0[50];//Rest Length

  mutable double normSegLenD[50];
  mutable double normSegLenV[50];

  const double SRheadgain = -120.535;
  mutable double externalinput[4] = {0.0, 0.0, 0.0, 0.0};

  const double NSEGSHEAD = 14;
  

}; // thresholdModel

#endif // #ifndef MODELTHRESHOLD_HH
